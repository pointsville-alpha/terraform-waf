provider "aws" {
  region = "us-west-2"
}
provider "random" {
  version = "3.0.1" # Use latest if possible. See https://github.com/terraform-providers/terraform-provider-random/releases
}
resource "aws_vpc" "vpc" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name = "terraform-example-vpc"
  }
}
resource "aws_internet_gateway" "gateway" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "terraform-example-internet-gateway"
  }
}
resource "aws_route" "route" {
  route_table_id         = aws_vpc.vpc.main_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.gateway.id
}
resource "aws_subnet" "main" {
  count                   = length(data.aws_availability_zones.available.names)
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = "10.0.${count.index}.0/24"
  map_public_ip_on_launch = true
  availability_zone       = element(data.aws_availability_zones.available.names, count.index)

  tags = {
    Name = "public-${element(data.aws_availability_zones.available.names, count.index)}"
  }
}
resource "aws_security_group" "default" {
  name        = "terraform_security_group"
  description = "Terraform example security group"
  vpc_id      = aws_vpc.vpc.id

  # Allow outbound internet access.
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "terraform-example-security-group"
  }
}
resource "aws_security_group" "alb" {
  name        = "terraform_alb_security_group"
  description = "Terraform load balancer security group"
  vpc_id      = aws_vpc.vpc.id

  /*ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = "${var.allowed_cidr_blocks}"
  }*/

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow all outbound traffic.
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "terraform-example-alb-security-group"
  }
}
resource "aws_alb" "alb" {
  name               = "terraform-example-alb"
  security_groups    = ["${aws_security_group.alb.id}"]
  subnets            = aws_subnet.main.*.id
  load_balancer_type = "application"
  ip_address_type    = "ipv4"
  internal           = false
  tags = {
    Name = "terraform-example-alb"
  }
}
resource "aws_alb_target_group" "group" {
  name     = "terraform-example-alb-target"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.vpc.id

  #stickiness {
  # type = "lb_cookie"
  #}

  # Alter the destination of the health check to be the login page.
  health_check {
    healthy_threshold   = 5
    unhealthy_threshold = 2
    interval            = 30
    timeout             = 5
    path                = "/"
    protocol            = "HTTP"
  }
}
resource "aws_alb_listener" "listener_http" {
  load_balancer_arn = aws_alb.alb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_alb_target_group.group.arn
    type             = "forward"
  }
}
module "owasp_top_10_rules" {
  #source = "git::git://git@github.com/traveloka/terraform-aws-waf-owasp-top-10-rules.git?ref=v1.0.0"
  source = "github.com/traveloka/terraform-aws-waf-owasp-top-10-rules"

  product_domain = "tsi"
  service_name   = "tsiwaf"
  environment    = "staging"
  description    = "OWASP Top 10 rules for tsiwaf"

  target_scope      = "regional" # [IMPORTANT] this variable value should be set to regional
  create_rule_group = "true"

  max_expected_uri_size          = "512"
  max_expected_query_string_size = "1024"
  max_expected_body_size         = "4096"
  max_expected_cookie_size       = "4093"

  csrf_expected_header = "x-csrf-token"
  csrf_expected_size   = "36"
}
resource "random_id" "this" {
  byte_length = "8"
}
resource "aws_wafregional_rate_based_rule" "rate_limiter_rule" {
  name        = "tsiwaf-rate-limiter-${random_id.this.hex}"
  metric_name = "tsiwafRateLimiter${random_id.this.hex}"

  rate_key   = "IP"
  rate_limit = "2000"
}
/*module "webacl_supporting_resources" {
  # This module is published on the registry: https://registry.terraform.io/modules/traveloka/waf-webacl-supporting-resources

  # Open the link above to see what the latest version is. Highly encouraged to use the latest version if possible.

  source = "github.com/traveloka/terraform-aws-waf-webacl-supporting-resources"

  product_domain = "tsi"
  service_name   = "tsiwaf"
  environment    = "staging"
  description    = "WebACL for tsiwaf"

  s3_logging_bucket = "<bucket-for-logging>" # Logging bucket should be in the same region as the bucket

  s3_kms_key_arn           = "xxxx"
  firehose_buffer_size     = "128"
  firehose_buffer_interval = "900"
}*/
resource "aws_wafregional_web_acl" "tsiwaf_webacl" {
  # The name or description of the web ACL.
  name = "tsiwaf-WebACL-${random_id.this.hex}"

  # The name or description for the Amazon CloudWatch metric of this web ACL.
  metric_name = "tsiwafWebACL${random_id.this.hex}"

  # Configuration block to enable WAF logging.
  /*logging_configuration {
    # Amazon Resource Name (ARN) of Kinesis Firehose Delivery Stream
    log_destination = module.webacl_supporting_resources.firehose_delivery_stream_arn
  }*/

  # Configuration block with action that you want AWS WAF to take 
  # when a request doesn't match the criteria in any of the rules 
  # that are associated with the web ACL.
  default_action {
    # Valid values are `ALLOW` and `BLOCK`.
    type = "ALLOW"
  }
  # Configuration blocks containing rules to associate with the web ACL and the settings for each rule.
  rule {
    # Specifies the order in which the rules in a WebACL are evaluated.
    # Rules with a lower value are evaluated before rules with a higher value.
    priority = "0"

    # ID of the associated WAF rule
    rule_id = module.owasp_top_10_rules.rule_group_id

    # Valid values are `GROUP`, `RATE_BASED`, and `REGULAR`
    # The rule type, either REGULAR, as defined by Rule, 
    # RATE_BASED, as defined by RateBasedRule, 
    # or GROUP, as defined by RuleGroup. 
    type = "GROUP"

    # Only used if type is `GROUP`.
    # Override the action that a group requests CloudFront or AWS WAF takes 
    # when a web request matches the conditions in the rule. 
    override_action {
      # Valid values are `NONE` and `COUNT`
      type = "NONE"
    }
  }
  rule {
    priority = "1"
    rule_id  = aws_wafregional_rate_based_rule.rate_limiter_rule.id
    type     = "RATE_BASED"

    # Only used if type is NOT `GROUP`.
    # The action that CloudFront or AWS WAF takes 
    # when a web request matches the conditions in the rule.
    action {
      # Valid values are `ALLOW`, `BLOCK`, and `COUNT`.
      type = "BLOCK"
    }
  }
}
resource "aws_wafregional_web_acl_association" "alb" {
  #count        = var.associate_alb ? 1 : 0
  resource_arn = aws_alb.alb.arn  #ARN of the ALB
  web_acl_id   = aws_wafregional_web_acl.tsiwaf_webacl.id
}
#"arn:aws:elasticloadbalancing:ap-southeast-1:<account-id>:loadbalancer/app/<lb-name>/<lb-id>"
/*resource "aws_wafregional_web_acl_association" "api" {
  resource_arn = "arn:aws:apigateway:ap-southeast-1::/restapis/<rest-api-id>/stages/<stage-name>" # ARN of the API Gateway stage
  web_acl_id   = aws_wafregional_web_acl.tsiwaf_webacl.id
}*/
